//
//  ChatViewController.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 03/05/22.
//

import UIKit

class ChatViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var messageTextfield: UITextField!
    
    var messages : [Message] = [
        Message(sender: "1@2.com", body: "Hey!"),
        Message(sender: "a@b.com", body: "Hello"),
        Message(sender: "1@2.com", body: "what's up!"),
        Message(sender: "1@2.com", body: "really long message to check what happens on multiple lines")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.register(UINib(nibName: K.cellNibName, bundle: nil), forCellReuseIdentifier: K.cellIdentifier)
        tableView.dataSource = self
    }
}


extension ChatViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: K.cellIdentifier, for: indexPath) as! MessageCell
        
        //MARK: -  default cell
//        var content = cell.defaultContentConfiguration()
//
//        // Configure content.
//        content.image = UIImage(systemName: "star")
//        content.text = messages[indexPath.row].body
//
//        // Customize appearance.
//        content.imageProperties.tintColor = .purple
//
//        cell.contentConfiguration = content
        
        
        //MARK: - custom design cell
        cell.label.text = messages[indexPath.row].body
        
        return cell
    }
    
    
}
