//
//  Message.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 03/05/22.
//

import Foundation

struct Message{
    let sender : String
    let body : String
}
