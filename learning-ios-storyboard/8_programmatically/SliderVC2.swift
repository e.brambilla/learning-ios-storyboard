//
//  SliderVC2.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 29/04/22.
//

import UIKit

class SliderVC2: UIViewController {
    var numberToShow : String = "0.0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .green

        let label = UILabel()
        label.text = numberToShow
        label.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
        view.addSubview(label)
    }

}
