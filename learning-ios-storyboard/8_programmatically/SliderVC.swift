//
//  SliderVC.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 29/04/22.
//

import UIKit

class SliderVC: UIViewController {
    @IBOutlet weak var firstNumber: UILabel!
    @IBOutlet weak var secondNumber: UILabel!
    
    @IBOutlet weak var firstNUmberSlider: UISlider!
    @IBOutlet weak var secondNumberSlider: UISlider!
    
    @IBAction func sliderMoved(_ sender: UISlider) {
        if sender.tag == 1 {
            self.firstNumber.text = String(format: "%.1f", sender.value)
        } else {
            self.secondNumber.text = String(format: "%.1f", sender.value)
        }
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        let finalNumber = firstNUmberSlider.value + secondNumberSlider.value
        
        let vc = SliderVC2()
        vc.numberToShow = String(format: "%.1f", finalNumber)
        self.present(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNumber.text = "Move Slider"
        secondNumber.text = "Move Slider"

        // Do any additional setup after loading the view.
    }
}
