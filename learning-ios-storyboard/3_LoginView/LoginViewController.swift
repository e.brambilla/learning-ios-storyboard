//
//  LoginViewController.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 22/04/22.
//

import Foundation
import UIKit


class LoginViewController: UIViewController {
    @IBOutlet var usernameErrorLabel: UILabel!
    @IBOutlet var passwordErrorLabel: UILabel!
    @IBOutlet var loginLabel: UILabel!
    
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBAction func buttonAction(_ sender: Any) {
        guard let username = usernameTextField.text else {return}
        guard let password = passwordTextField.text else {return}
        
        let validEmail = username.isValidEmail()
        if !validEmail {
            usernameErrorLabel.text = "Invalid Username!"
            usernameErrorLabel.isHidden = false
        } else {
            usernameErrorLabel.isHidden = true
        }
        
        let validPassword = self.isValidPassword(password)
        if !validPassword{
            passwordErrorLabel.text = "Invalid Password"
            passwordErrorLabel.isHidden = false
        } else{
            passwordErrorLabel.isHidden = true
        }
        
        if validEmail && validPassword{
            self.loginLabel.isHidden = false
            self.loginLabel.text = "Login Success 🎉"
        }
        
    }
    
    //MARK: -  ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameErrorLabel.isHidden = true
        passwordErrorLabel.isHidden = true
        self.loginLabel.isHidden = true
        passwordTextField.isSecureTextEntry = true
    }
    
    //MARK: -  Mathods
    
    func isValidPassword(_ text: String) -> Bool{
        let validLeghth = text.count >= 8
        let validNumber = text.hasNumberCharacter()
        let validLowercased = text.hasLowercasedCharacters()
        
        return validLeghth && validNumber && validLowercased
    }

}



extension String {
    func isValidEmail() -> Bool {
        return stringFulfillsRegex(regex: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
    }
    
    func hasLowercasedCharacters() -> Bool {
        return stringFulfillsRegex(regex:  ".*[a-z]+.*")
    }

    func hasNumberCharacter() -> Bool {
        return stringFulfillsRegex(regex: ".*[0-9].*")
    }
    
    private func stringFulfillsRegex(regex: String) -> Bool {
        let texttest = NSPredicate(format: "SELF MATCHES %@", regex)
        guard texttest.evaluate(with: self) else {
            return false
        }
        return true
    }
    
}
