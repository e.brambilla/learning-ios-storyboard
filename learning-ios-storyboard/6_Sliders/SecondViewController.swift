//
//  SecondViewController.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 29/04/22.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        print("view Did Load - 2")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear (animated)
        // Add your code here
        print("viewwillAppear - 2")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewdidappered - 2")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear - 2")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear - 2")
    }

}
