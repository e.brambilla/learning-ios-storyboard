//
//  ViewController2.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 21/04/22.
//

import UIKit

class ViewControllerSlider: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("view Did Load")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear (animated)
        // Add your code here
        print("viewwillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewdidappered")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")
    }
    
}

