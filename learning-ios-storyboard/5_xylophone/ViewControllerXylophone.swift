//
//  ViewController.swift
//  Xylophone
//
//  Created by Angela Yu on 28/06/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit
import AVFoundation

class ViewControllerXylophone: UIViewController {


    var player: AVAudioPlayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func keyPressed(_ sender: UIButton) {
        
        if let buttonTitle = sender.titleLabel?.text{
            playSound(note: buttonTitle.capitalized)
        }
        
    }
    
    func playSound(note: String) {
        guard let url = Bundle.main.url(forResource: note, withExtension: "wav") else {
            print("sound not found")
            return }
        player = try! AVAudioPlayer(contentsOf: url)
        player.play()
        
    }
}


