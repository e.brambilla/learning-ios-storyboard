//
//  CustomCollectionView.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 17/05/22.
//

import UIKit

class CustomCollectionView: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var dataSource : [[CellProtocol]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(.init(nibName: CollectionViewCell.cellName , bundle: nil), forCellWithReuseIdentifier: CollectionViewCell.cellIdentifier)
        makeDataSource()
        
        collectionView.reloadData()
    }

    func makeDataSource(){
        let items = [
            CollectionCellData(colorLeft: .green, colorRight: .systemTeal),
            CollectionCellData(colorLeft: .purple, colorRight: .systemPurple),
            CollectionCellData(colorLeft: .systemPink, colorRight: .blue),
            CollectionCellData(colorLeft: .green, colorRight: .systemTeal),
            CollectionCellData(colorLeft: .purple, colorRight: .systemPurple),
            CollectionCellData(colorLeft: .systemPink, colorRight: .blue),
        ]
        self.dataSource.append(items)
    }

}

extension CustomCollectionView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSource[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let section = dataSource[indexPath.section]
        let item = section[indexPath.item] as! CollectionCellData
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.cellIdentifier, for: indexPath) as! CollectionViewCell
        cell.configure(colorLeft: item.colorLeft, colorRight: item.colorRight)
        return cell
    }
}

extension CustomCollectionView : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let leftAndRightPaddings: CGFloat = 20.0
        let numberOfItemsPerRow: CGFloat = 2.0

        let width = (collectionView.frame.width-leftAndRightPaddings)/numberOfItemsPerRow
//        let width = (collectionView.frame.width)/numberOfItemsPerRow
        return CGSize(width: width, height: width) // You can change width and height here as pr your requirement

    }
}






