//
//  CollectionViewCell.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 17/05/22.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    static let cellIdentifier: String = "collectionViewCell"
    static let cellName: String = "CollectionViewCell"
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var leftCellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leftCellView.layer.cornerRadius = 8
        label.text = ""
    }
    
    func configure(colorLeft: UIColor, colorRight: UIColor){
        self.leftCellView.backgroundColor = colorLeft
    }
    
    func configure(color: UIColor, labelText: String){
        self.leftCellView.backgroundColor = color
        self.label.text = labelText
    }

}
