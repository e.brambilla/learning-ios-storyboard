//
//  CollectionViewModel.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 17/05/22.
//

import Foundation
import UIKit


struct CollectionCellData: CellProtocol{
    var cellIdentifier: String {
        return ProfileCell.cellIdentifier
    }
    var cellName: String{
        return ProfileCell.cellName
    }
    
    var colorLeft : UIColor
    var colorRight : UIColor
}


struct NetflixCellData: CellProtocol{
    var cellIdentifier: String {
        return ProfileCell.cellIdentifier
    }
    var cellName: String{
        return ProfileCell.cellName
    }
    
    var color : UIColor
    var text : String
}
