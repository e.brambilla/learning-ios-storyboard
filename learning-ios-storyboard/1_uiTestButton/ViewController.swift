//
//  ViewController.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 21/04/22.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView1: UIImageView!
    @IBOutlet weak var mainLabel2: UILabel!

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var toggle: UISwitch!
    @IBAction func switchToggle(_ sender: UISwitch) {
        print("the switch is \(sender.isOn)")
    }
    
    @IBAction func sliderValue(_ sender: UISlider) {
        print(sender.value)
    }
    
    
    @IBAction func textChanged(_ sender: UITextField) {
        if let text = sender.text {
            mainLabel2.text = text
        }
    }
    
    @IBAction func respondToLongTap(_ sender: UILongPressGestureRecognizer) {
        let location = sender.location(in: view)
        print(location)
        
        self.toggle.isOn.toggle()
    }
    
    
    
//    @IBAction func changeTitle(_ sender: Any) {
//        mainLabel2.text = "Button Clicked"
//        imageView1?.image = #imageLiteral(resourceName: "smile")
//    }
    
    let labelProgrammatic = UILabel(frame: CGRect(x:16, y:16, width: 200, height: 44))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mainLabel2.font = UIFont.systemFont(ofSize: 22)
        mainLabel2.text = """
            test label
            """
        
        
        button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        print("button Tapped")
        imageView1?.image = #imageLiteral(resourceName: "smile")
    }
    
    
    
}

