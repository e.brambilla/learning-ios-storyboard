//
//  NetflixViewController.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 17/05/22.
//

import UIKit

class NetflixViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource : [[CellProtocol]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.makeDataSource()
        
        tableView.dataSource = self
        tableView.reloadData()
    }
    
    private func registerCells(){
        tableView.register(UINib(nibName: NetflixTableViewCell.cellName, bundle: nil), forCellReuseIdentifier: NetflixTableViewCell.cellIdentifier)
    }
    
    func makeDataSource(){
        let items = [
            NetflixCellData(color: .red, text: "Primo Film"),
            NetflixCellData(color: .green, text: "Secondo Film"),
            NetflixCellData(color: .purple, text: "Terzo Film"),
            NetflixCellData(color: .red, text: "Quarto Film"),
        ]
        self.dataSource.append(items)
    }
    
}

extension NetflixViewController : UITableViewDataSource{
//    func numberOfSections(in tableView: UITableView) -> Int {
//        dataSource.count
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NetflixTableViewCell.cellIdentifier, for: indexPath) as! NetflixTableViewCell
        cell.configure(dataSource: dataSource, title: "Movies")
        
        return cell
    }
    
    
}

