//
//  NetflixTableViewCell.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 17/05/22.
//

import UIKit

class NetflixTableViewCell: UITableViewCell {
    
    var dataSource : [[CellProtocol]] = []
    static let cellIdentifier = "NetflixCellIdentifier"
    static let cellName = "NetflixTableViewCell"
    
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        collectionView.register(UINib(nibName: CollectionViewCell.cellName, bundle: nil), forCellWithReuseIdentifier: CollectionViewCell.cellIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(dataSource: [[CellProtocol]], title: String){
        self.dataSource = dataSource
        self.label.text = title
        collectionView.reloadData()
    }
}

extension NetflixTableViewCell : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        dataSource[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let section = dataSource[indexPath.section]
        let item = section[indexPath.item] as! NetflixCellData
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.cellIdentifier, for: indexPath) as! CollectionViewCell
        cell.configure(color: item.color, labelText: item.text)
        return cell
    }
    
}

extension NetflixTableViewCell : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let leftAndRightPaddings: CGFloat = 20.0
        let numberOfItemsPerRow: CGFloat = 2.0

        let width = (collectionView.frame.width/4-leftAndRightPaddings)/numberOfItemsPerRow
//        let width = (collectionView.frame.width)/numberOfItemsPerRow
        return CGSize(width: width, height: width) // You can change width and height here as pr your requirement
    }
    
}
