//
//  TabBarViewController.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 29/04/22.
//

import UIKit

class TabBarViewController: UIViewController {
    var tabCount = 0
    
    @IBAction func buttonTap(_ sender: Any) {
        tabCount += 1
        tabBarItem.badgeValue = "\(tabCount)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tabCount = 0
        tabBarItem.badgeValue = nil
    }

    

}
