//
//  CustomTableViewController.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 09/05/22.
//

import UIKit

class CustomTableViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var dataSource : [[CellProtocol]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.registerCells()
        tableView.dataSource = self
        
        self.makeDataSource()
        tableView.reloadData()
    }
    
    
    private func registerCells(){
        tableView.register(UINib(nibName: ProfileCell.cellName, bundle: nil), forCellReuseIdentifier: ProfileCell.cellIdentifier)
        tableView.register(UINib(nibName: NotesCell.cellName, bundle: nil), forCellReuseIdentifier: NotesCell.cellIdentifier)
        tableView.register(UINib(nibName: CalendarCell.cellName, bundle: nil), forCellReuseIdentifier: CalendarCell.cellIdentifier)
    }
    

    private func makeDataSource(){
        let profile = ProfileCellData(
            image: UIImage(systemName: "person.crop.circle.fill")!,
            label: "John Doe"
        )
        
        let calendarCells = [
            CalendarCellData(title: "CS-101 Python, Exercise 6,", text: "Data Structures"),
            CalendarCellData(title: "CS-101 Python, Exercise 5,", text: "Modules & Functions"),
            CalendarCellData(title: "CS-101 Python, Exercise 4,", text: "Control Flow (if, while and for loop)"),
            CalendarCellData(title: "CS-101 Python, Exercise 3,", text: "Variables and Constants"),
            CalendarCellData(title: "CS-101 Python, Exercise 3,", text: "Variables and Constants"),
            CalendarCellData(title: "CS-101 Python, Exercise 3,", text: "Variables and Constants")
        ]
        
        let notes = NotesCellData(
            sectionLeft: .init(title: "Notes ", text: "\(calendarCells.count) dasghfdasgdfas hdfaghfdhafhgfasf fhjffjhfh dfasjkhdkashkdahs dadjh jkdhasjk dhkaj hdjkash djkahjkahsjkdhajkhd jkahsdjkahjkdh ajkdhkahdaj Y"),
            sectionRight: .init(title: "Imporant ", text: "0")
        )
        
        dataSource.append([profile])
        dataSource.append([notes])
        dataSource.append(calendarCells)
    }

}

extension CustomTableViewController : UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = dataSource[indexPath.section]
        let row = section[indexPath.row]
        
        
        switch row{
        case let profile as ProfileCellData:
            let cell = tableView.dequeueReusableCell(withIdentifier: profile.cellIdentifier, for: indexPath) as! ProfileCell
            cell.configure(profile)
            return cell
            
        case let notes as NotesCellData:
            let cell = tableView.dequeueReusableCell(withIdentifier: notes.cellIdentifier, for: indexPath) as! NotesCell
            cell.configure(notes)
            return cell
            
        case let appointment as CalendarCellData:
            let cell = tableView.dequeueReusableCell(withIdentifier: appointment.cellIdentifier, for: indexPath) as! CalendarCell
            cell.configure(appointment, index: indexPath.row)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}
