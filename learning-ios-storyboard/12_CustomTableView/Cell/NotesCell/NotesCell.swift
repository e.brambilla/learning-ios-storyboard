//
//  NotesCell.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 09/05/22.
//

import UIKit

class NotesCell: UITableViewCell {
    static let cellIdentifier = "notesCell"
    static let cellName = "NotesCell"
    
    @IBOutlet weak var titleLeft: UILabel!
    @IBOutlet weak var titleRight: UILabel!
    @IBOutlet weak var textLeft: UILabel!
    @IBOutlet weak var textRight: UILabel!
    
    
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leftView.layer.cornerRadius = 8
        rightView.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(titleLeft : String, textLeft: String, titleRight: String, textRight: String) {
        self.titleLeft.text = titleLeft
        self.titleRight.text = titleRight
        
        self.textLeft.text = textLeft
        self.textRight.text = textRight
    }
    
    func configure(_ cell : NotesCellData){
        configure(
            titleLeft: cell.sectionLeft.title,
            textLeft: cell.sectionLeft.text,
            titleRight: cell.sectionRight.title,
            textRight: cell.sectionRight.text
        )
    }
    
}
