//
//  TableViewCell.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 09/05/22.
//

import UIKit

class CalendarCell: UITableViewCell {
    static let cellIdentifier = "calendarCell"
    static let cellName = "CalendarCell"
    
    
    @IBOutlet weak var colorCal: UIView!
    @IBOutlet weak var titleCal: UILabel!
    @IBOutlet weak var textCal: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(colorCal : UIColor, titleCal: String, textCal: String) {
        self.colorCal.backgroundColor = colorCal
        self.titleCal.text = titleCal
        self.textCal.text = textCal
    }
    
    func configure(_ cell: CalendarCellData, index : Int ){
        configure(
            colorCal: colorFinder(withIndex: index),
            titleCal: cell.title,
            textCal: cell.text)
    }
    
    func colorFinder(withIndex index : Int) -> UIColor{
        switch index{
        case 0 :
            return .red
        case 1 :
            return .tintColor
        case 2 :
            return .gray
        default:
            return colorFinder(withIndex: index-3)
        }
    }
    
}
