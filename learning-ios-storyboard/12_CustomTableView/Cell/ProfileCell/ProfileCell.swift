//
//  ProfileCell.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 09/05/22.
//

import UIKit

class ProfileCell: UITableViewCell {
    static let cellIdentifier = "profileCell"
    static let cellName = "ProfileCell"
    
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var labelProfile: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(image : UIImage, label: String) {
        imageProfile.image = image
        labelProfile.text = label
    }
    
    func configure(_ cell: ProfileCellData){
        configure(image: cell.image, label: cell.label)
    }
    
}
