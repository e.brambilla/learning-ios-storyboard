//
//  ProfileCell.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 09/05/22.
//

import Foundation
import UIKit


struct ProfileCellData: CellProtocol{
    var cellIdentifier: String {
        return ProfileCell.cellIdentifier
    }
    var cellName: String{
        return ProfileCell.cellName
    }
    
    var image : UIImage
    var label: String
}


struct NotesCellData: CellProtocol{
    var cellIdentifier: String{
        return NotesCell.cellIdentifier
    }
    var cellName: String{
        return NotesCell.cellName
    }
    
    struct Section{
        var title : String
        var text : String
    }
    
    var sectionLeft : Section
    var sectionRight : Section
}


struct CalendarCellData: CellProtocol{
    var cellIdentifier: String{
        return CalendarCell.cellIdentifier
    }
    var cellName: String{
        return CalendarCell.cellName
    }
    
    var title : String
    var text : String
}
