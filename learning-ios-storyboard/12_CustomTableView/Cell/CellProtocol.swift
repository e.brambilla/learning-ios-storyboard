//
//  CellProtocol.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 09/05/22.
//

import Foundation


protocol CellProtocol {
    var cellIdentifier : String { get }
    var cellName : String { get }
}
