//
//  uiTableViewCell.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 05/05/22.
//

import UIKit

class Cell2: UITableViewCell {
    static let cellIdentifier = "CellFromNib"
    static let cellName = "Cell2"
    
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
    func configureCell(title: String, description: String) {
        self.labelTitle.text = title
        self.labelTitle.font = .systemFont(ofSize: 16, weight: .black)
        self.labelTitle.numberOfLines = 1
        self.labelTitle.textColor = UIColor.black

        
        self.labelDescription.text = description
        self.labelDescription.font = .systemFont(ofSize: 16, weight: .regular)
        self.labelDescription.numberOfLines = 0
        self.labelDescription.textColor = UIColor.black
    }
    
    func configureCell(messageUI: MessageUI){
        configureCell(title: messageUI.title, description: messageUI.subtitle)
    }

}

extension Cell2 {
    struct MessageUI : CellProtocol {
        var cellIdentifier: String {
            return Cell2.cellIdentifier
        }
        var cellName: String {
            return Cell2.cellName
        }
        
        var title : String
        var subtitle : String
    }
}
