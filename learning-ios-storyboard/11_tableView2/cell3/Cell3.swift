//
//  Cell3.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 05/05/22.
//

import UIKit

class Cell3: UITableViewCell {
    static let cellIdentifier = "Cell3Identifier"
    static let cellName = "Cell3"
    
    @IBOutlet weak var imageLeft: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labeldescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(image: UIImage, title: String, description: String){
        self.imageLeft.image = image
        self.labelTitle.text = title
        self.labelTitle.tintColor = .link
        self.labelTitle.font = .systemFont(ofSize: 16, weight: .black)
        
        self.labeldescription.text = description
        self.labeldescription.numberOfLines = 0
    }
    
    func configure(imageUI : ImageUI){
        configure(image: imageUI.image, title: imageUI.title, description: imageUI.description)
    }
    
}

extension Cell3{
    struct ImageUI : CellProtocol{
        
        var cellIdentifier: String {
            return Cell3.cellIdentifier
        }
        var cellName: String {
            return Cell3.cellName
        }
        
        var image : UIImage
        var title : String
        var description: String
    }
}
