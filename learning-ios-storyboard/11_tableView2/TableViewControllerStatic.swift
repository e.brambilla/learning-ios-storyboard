//
//  TableViewController2.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 04/05/22.
//

import UIKit

class TableViewControllerStatic: UITableViewController {
    let cellIdentifierPrimary = Cell2.cellIdentifier
    let cellName = Cell2.cellName
    
    struct TableRowContent {
        let title : String
        let text : String
    }
    
    let rows : [TableRowContent] = [
        .init(title: "1st message that stays in one line, this is dhajksdaldkahdajhd afahskflahfahfdklah", text: "descrizione 1 "),
        .init(title: "2nd message", text: "descrizione 2"),
        .init(title: "3rd message", text: "descrizione 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dolor ipsum, ornare a pulvinar ut, lacinia sit amet nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque at sem a nulla elementum dignissim quis id dui. Maecenas vestibulum nulla nec nunc scelerisque pellentesque. Nulla vitae fermentum libero. Donec tincidunt rutrum massa vel fringilla. Donec nibh ante, molestie non ultricies eget, vehicula vel arcu. Mauris orci libero, porttitor hendrerit nibh sit amet, dictum dictum dui. Integer id auctor lectus. Pellentesque iaculis ullamcorper malesuada"),
        .init(title: "4th message", text: "descrizione 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dolor ipsum, ornare a pulvinar ut, lacinia sit amet nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque at sem a nulla elementum dignissim quis id dui. Maecenas vestibulum nulla nec nunc scelerisque pellentesque. Nulla vitae fermentum libero. Donec tincidunt rutrum massa vel fringilla. Donec nibh ante, molestie non ultricies eget, vehicula vel arcu. Mauris orci libero, porttitor hendrerit nibh sit amet, dictum dictum dui. Integer id auctor lectus. Pellentesque iaculis ullamcorper malesuada")
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: self.cellName, bundle: nil), forCellReuseIdentifier: self.cellIdentifierPrimary)
        tableView.dataSource = self
        tableView.delegate = self
    }
}


extension TableViewControllerStatic {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("section number: \(section)")
        
        switch section{
        case 0,2,3,4 :
            return 1
        case 1 :
            return 2
        default:
            return 0
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifierPrimary, for: indexPath) as! Cell2
        cell.configureCell(title: "title \(indexPath.row) in section \(indexPath.section)", description: "description \(indexPath.row)")
        return cell
    }
}


extension TableViewControllerStatic{
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row \(indexPath.row)")
    }
}
