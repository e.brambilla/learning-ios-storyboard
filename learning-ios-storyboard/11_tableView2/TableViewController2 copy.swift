//
//  TableViewController2.swift
//  learning-ios-storyboard
//
//  Created by Emilio Brambilla on 04/05/22.
//

import UIKit

class TableViewControllerDynamic: UIViewController {
    let cellIdentifierPrimary = Cell2.cellIdentifier
    let cellName = Cell2.cellName
    
    let cellIndetifierSecondary = Cell3.cellIdentifier
    let cellNameSecondary = Cell3.cellName
    
    struct TableRowContent {
        let title : String
        let text : String
    }
    
    var dataSource : [[CellProtocol]] = []
    
//    let rows : [TableRowContent] = [
//        .init(title: "1st message that stays in one line, this is dhajksdaldkahdajhd afahskflahfahfdklah", text: "descrizione 1 "),
//        .init(title: "2nd message", text: "descrizione 2 nec nunc scelerisque pellentesque. Nulla vitae fermentum libero. Donec tincidunt rutrum massa vel fringilla. Donec nibh ante, molestie non ultricies eget, vehicula vel arcu. Mauris orci libero, porttitor hendrerit nibh sit amet, dictum dictum dui. Integer id auctor lectus. Pellentesque iaculis ullamcorper malesuada"),
//        .init(title: "3rd message", text: "descrizione 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dolor ipsum, ornare a pulvinar ut, lacinia sit amet nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque at sem a nulla elementum dignissim quis id dui. Maecenas vestibulum nulla nec nunc scelerisque pellentesque. Nulla vitae fermentum libero. Donec tincidunt rutrum massa vel fringilla. Donec nibh ante, molestie non ultricies eget, vehicula vel arcu. Mauris orci libero, porttitor hendrerit nibh sit amet, dictum dictum dui. Integer id auctor lectus. Pellentesque iaculis ullamcorper malesuada"),
//        .init(title: "4th message", text: "descrizione 3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dolor ipsum, ornare a pulvinar ut, lacinia sit amet nisl. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque at sem a nulla elementum dignissim quis id dui. Maecenas vestibulum nulla nec nunc scelerisque pellentesque. Nulla vitae fermentum libero. Donec tincidunt rutrum massa vel fringilla. Donec nibh ante, molestie non ultricies eget, vehicula vel arcu. Mauris orci libero, porttitor hendrerit nibh sit amet, dictum dictum dui. Integer id auctor lectus. Pellentesque iaculis ullamcorper malesuada")
//    ]
    

    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: self.cellName, bundle: nil), forCellReuseIdentifier: self.cellIdentifierPrimary)
        tableView.register(UINib(nibName: self.cellNameSecondary, bundle: nil), forCellReuseIdentifier: self.cellIndetifierSecondary)
        tableView.dataSource = self
        tableView.delegate = self
        
        makeDataSource()
        tableView.reloadData()
    }
    
    
    private func makeDataSource() {
        let section0 : [CellProtocol] = [
            Cell3.ImageUI(image: UIImage(systemName: "square.and.arrow.up")!, title: "Image1", description: "test")
        ]

        let section1 : [CellProtocol] = [
            Cell2.MessageUI(title: "Message1", subtitle: "test"),
            Cell2.MessageUI(title: "Message2", subtitle: "test"),
            Cell2.MessageUI(title: "Message3", subtitle: "test"),
        ]

        self.dataSource = [section1, section0, section1]
    }
}


extension TableViewControllerDynamic: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource[section].count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = dataSource[indexPath.section]
        let row = section[indexPath.row]
        
        switch row {
        case let messageUI as Cell2.MessageUI:
            let cell = tableView.dequeueReusableCell(withIdentifier: messageUI.cellIdentifier, for: indexPath) as! Cell2
            cell.configureCell(messageUI: messageUI)
            return cell
            
        case let imageUI as Cell3.ImageUI:
            let cell = tableView.dequeueReusableCell(withIdentifier: imageUI.cellIdentifier, for: indexPath) as! Cell3
            cell.configure(imageUI: imageUI)
            return cell
        
        default:
            return UITableViewCell()
        }
    }
}


extension TableViewControllerDynamic: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row \(indexPath.row)")
    }
}
