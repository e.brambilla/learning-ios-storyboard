import UIKit

protocol CellProtocol {
    var cellIdentifier : String { get }
    var nib : String { get }
}

struct MessageUI : CellProtocol {
    var cellIdentifier: String {
        return ""
    }
    var nib: String {return ""}
    
    var title : String
    var subtitle : String
}

struct ImageUI : CellProtocol{
    var cellIdentifier: String {
        return ""
    }
    var nib: String {return ""}
    
    var image : String
}


let array1 : [CellProtocol] = [
    ImageUI(image: "")
]

let array2 : [CellProtocol] = [
    MessageUI(title: "", subtitle: "")
]

let dataSource : [[CellProtocol]] = [array1, array2]
